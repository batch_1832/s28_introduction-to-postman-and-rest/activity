// console.log("Hello World")

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json))


//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => {
	json.map(posts => console.log(posts.title));
})